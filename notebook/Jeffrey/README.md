9/12/23
We decided to use the Rasberry Pi Pico W due to its wifi capabilities. Specifically, it would handle all of our bluetooth communication from the controller and output video to our computer via wifi. Our power source would need to be between 1.8-5.5V, but we will most likely opt towards a 5V battery due to it being a standard voltage level for many logic chips. For the Pico W, the cameras [here](https://www.arducam.com/raspberry-pi-pico-camera-modules/) are compatible and we will choose one of them.


9/24/23
met up to discuss the PCB design of our system. We decided to use the [Attiny 85](https://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-7669-ATtiny25-45-85-Appendix-B-Automotive-Specification-at-1.8V_Datasheet.pdf) microcontroller, and spent the meeting trying to figure out how the microcontroller works. Since the Attiny 85 doesn't have enough logic pins to run all the [H-bridges](https://www.ti.com/lit/ds/symlink/drv8838.pdf?ts=1702036978976) we plan on using we also tried to figure out how to decode serial outputs from one pin into several different hbridges to run our motors. 


10/9/23
converted the PCB schematic into an actual PCB design, left open ports for the rasberry pi as well as all the motor connections. Planning on having a meeting tomorrow about the PCB design and how to improve on it with Jason.


10/10/23
met up with Jason during office hours, my PCB design has a lot of mistakes. My power connections are too thin, I need to change that. I was advised to make copper plates for the power bus, which makes sense. I was told that I was just making life harder for myself with the serial to bitshifter system I was currently designing, and since this was the second time I had gotten that bit of advice I switched the microcontroller to the [Attiny 404](https://ww1.microchip.com/downloads/en/devicedoc/50002687a.pdf). This Microcontroller was still in the Attiny family so I thought it would be familiar to work with, and it had enough IO pins to directly run all the h bridges. Also met up with my team to discuss next steps and what to work on. We talked about the physical design of our drone, and how the size of the PCB will affect the design. We have an upper estimate on the size of the PCB now (~100mm x ~60mm), so we are currently working around that.


10/17/23
Finished first draft of the PCB and put out an order for it (61x35.6mm). 


11/2/23
Baked the components to our PCB. The outputs of the H-bridges are exposed plates because I thought it would be easier to wire the motors to the system that way. The 6 pin input on the right is for programming the microcontroller via SPI. The two Vin pins at the top are for two 3.7 V LIon batteries. We needed this to achieve 7.4 V for the hbridges to power the motor properly. There is a Linear regulator to reduce the voltage to 5V to power our logic elements. The three pads between the microcontroller and the SPI pins are meant to be the inputs for the spi communication between the Raspberry Pi Zero W and the microcontroller.

![](PCB_img.jpg)


11/14/23
I learned today the Attiny 404 needs to be initially programmed via UPDI instead of SPI, which means I need to set up a UPDI programmer and connect a programming wire to the board.

![](updi_prgm.png)


11/18/23
I have spent several days trying to get the UPDI programmer to work, and have made many steps both forward and backward. The first problem was that the UPDI pin isn't wired to any of the 4 pins meant for SPI, so I soldered a wire directly onto the pin in question. Another problem was that I learnt that I set up the software for the programmer incorrectly, which was simple enough to fix once I realized my mistake. The problem was that I didn't realize that was the issue for the longest time because the USB port for the lab computer I was using were garbage, and not transmitting the port data I needed to program. I only realized this was the issue when the keyboard plugged into the computer stopped functioning. Once I plugged the UPDI programmer into a USB port that wasn't broken, I was able to figure out my software problems. 
Unfortunately, I also accidentally bricked 2 Attiny 404s by switching GND and PWR, which were all that we had. So I needed to order more attinys and wait.Also at this point our deadline was so close that we didn't have time to order a new PCB. We decided that the first iteration PCB would suffice, and we would frankenstein it if we had to.
![](updi_hack.jpg)


11/20/23
Our initial board that we reflowed all the parts onto had gotten pretty grimy due to subsequent soldering/desoldering actions. The flux on the board was making it difficult to have the pins of our components touch the pad, so we decided to reflow oven a new board. Unfortunately our stencil was lost, so we had to order another one. We were already on a time crunch and this extra wait was not appreciated, but what could we do.


11/24/23
Josh dipped from the project, which is annoying. 


11/25/23
Extra microcontrollers came in, so I tried programming those. Was extra careful to not break the microcontrollers but I somehow broke the Arduino Nano everyboard I was using. At this point, I think it's a talent. The image below has the remnants of the UPDI programmer without the capacitor/resistor peripherals.
![](bricked_ANano.jpg)


11/27/23
Our stencil came in and we got a bunch of work done. We wanted to program the chip first before putting it on the board just in case the programming went wrong. We didnt want to go through the trouble of scraping the microprocessor off the board again. We ended up holding the power, ground, and UPDI programming wire to the pins with our hands and programming it that way, which worked suprisingly well. We finally got the 404 to program via UPDI.

11/28/23
We realized part of the problem with our previous board H-bridges not working was due to poor soldering, so we were really careful when we soldered everything to our new board. Once everything was on the board and stable, I wrote a truth table for the 3 bit inputs that the raspberry pi would be sending us, and the motor outputs the microcontroller would be outputting from them. We got logic working today.

11/29/23 + 11/30/23
Worked all night and into the morning of the next day getting everything on the drone and working (somewhat) stable-y. We had a lot of trouble with the motor pads coming from the hbridges, and one of them even peeled off. We had to hot glue a pin down as a temporary fix, but it was spotty and didnt work well. Nevertheless we managed to get everything on the drone and had it streaming to the computer and driving around via bluetooth controller.
![](drone_vid.mp4)
