#define IN_1_1 PIN_PA0
#define IN_1_2 PIN_PA5
#define IN_2_1 PIN_PA6
#define IN_2_2 PIN_PA7
#define IN_3_1 PIN_PB0
#define IN_3_2 PIN_PB1

#include <SPI.h>

void setup() {
    SPI.begin();
}

void loop() {
    delay(50);
    
    SPI.beginTransaction(SPISettings(14000000, MSBFIRST, SPI_MODE0));

    int receivedVal16 = SPI.transfer16(0);

    SPI.endTransaction();


    IN_1_1 = receivedVal16 % 2;
    receivedVal16 /= 2;
    IN_1_2 = receivedVal16 % 2;
    receivedVal16 /= 2;

    IN_2_1 = receivedVal16 % 2;
    receivedVal16 /= 2;
    IN_2_2 = receivedVal16 % 2;
    receivedVal16 /= 2;

    IN_3_1 = receivedVal16 % 2;
    receivedVal16 /= 2;
    IN_3_2 = receivedVal16 % 2;
    receivedVal16 /= 2;

    
}